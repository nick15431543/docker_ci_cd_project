from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

conf = SparkConf().setAppName('My PySpark App').setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)

spark = SparkSession.builder.config(conf=conf).getOrCreate()
data = [1, 2, 3, 4, 5, 6, 7]
rdd = sc.parallelize(data)
multiplied_rdd = rdd.reduce(lambda x, y: x * y)
print("Product", multiplied_rdd)
spark.stop()