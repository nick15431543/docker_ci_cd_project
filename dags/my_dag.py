from datetime import datetime, timedelta

from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag =  DAG(
    "my_dag",
    default_args=default_args,
    description='DAG',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2023, 10, 16),
    catchup=False
)
    
spark_job = SparkSubmitOperator(
    task_id='test',
    application='/opt/airflow/spark/test.py',
    name='test',
    conn_id='spark_local',
    dag=dag
)

spark_job
